/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/8.5.50
 * Generated at: 2020-03-31 09:31:44 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.views;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class service_005fcenter_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    final java.lang.String _jspx_method = request.getMethod();
    if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
      return;
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\r\n");
      out.write("<meta name=\"viewport\"\r\n");
      out.write("\tcontent=\"user-scalable=no,width=device-width, initial-scale=1.0\" />\r\n");
      out.write("\r\n");
      out.write("<title>GOODLUXE :: 굿럭스</title>\r\n");
      out.write("\r\n");
      out.write("<link\r\n");
      out.write("\thref=\"https://fonts.googleapis.com/css?family=Nanum+Gothic&display=swap&subset=korean\"\r\n");
      out.write("\trel=\"stylesheet\">\r\n");
      out.write("<link rel=\"stylesheet\"\r\n");
      out.write("\thref=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("/resources/css/goodluxe-header.css\" />\r\n");
      out.write("<link rel=\"stylesheet\"\r\n");
      out.write("\thref=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("/resources/css/login.css\" />\r\n");
      out.write("<link rel=\"stylesheet\"\r\n");
      out.write("\thref=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("/resources/css/goodluxe-footer.css\" />\r\n");
      out.write("<script type=\"text/javascript\"\r\n");
      out.write("\tsrc=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("/resources/js/jquery-3.4.1.js\"></script>\r\n");
      out.write("<script type=\"text/javascript\"\r\n");
      out.write("\tsrc=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("/resources/js/jquery.easing.1.3.js\"></script>\r\n");
      out.write("<script>\r\n");
      out.write("\t$(document).ready(function() {\r\n");
      out.write("\t\t$(\"#header\").load(\"header.do\");\r\n");
      out.write("\t\t$(\"#nav_bar\").load(\"navBar.do\");\r\n");
      out.write("\t\t$(\"#login_box\").load(\"loginBox.do\");\r\n");
      out.write("\t\t$(\"#footer\").load(\"footer.do\");\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t$(\".scmt\").load(\"scMenu.do\", function(){\r\n");
      out.write("\t\t\t$('.scmt').children('a').eq(0).addClass('current');\r\n");
      out.write("\t\t});\r\n");
      out.write("\r\n");
      out.write("\t});\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("<!-- order_and_shipping -->\r\n");
      out.write("<link rel=\"stylesheet\"\r\n");
      out.write("\thref=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("/resources/css/service_center.css\" />\r\n");
      out.write("<script type=\"text/javascript\"\r\n");
      out.write("\tsrc=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("/resources/js/sc_faq.js\"></script>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body class=\"\">\r\n");
      out.write("\t<header id=\"header\"></header>\r\n");
      out.write("\r\n");
      out.write("\t<nav id=\"nav_bar\"></nav>\r\n");
      out.write("\r\n");
      out.write("\t<div class=\"login_bg\" id=\"login_box\"></div>\r\n");
      out.write("\r\n");
      out.write("    <section id=\"container\">\r\n");
      out.write("        <br />\r\n");
      out.write("        <div id=\"main\">\r\n");
      out.write("            <section class=\"service_center_container\">\r\n");
      out.write("                <div class=\"service_center_main\">\r\n");
      out.write("                    <p style=\"text-align: left; margin: 18px 0 16px 14px;font-size: 24px;font-weight:none;\">| 고객센터</p>\r\n");
      out.write("\r\n");
      out.write("                    <!--고객센터 메뉴 탭-->\r\n");
      out.write("                    <ul class=\"service_center_menu_tabs scmt\"></ul>\r\n");
      out.write("                    <hr id=\"center_tab-line\" class=\"center_tab-line\">\r\n");
      out.write("\r\n");
      out.write("                    <!--고객센터 탭 1번 F&Q-->\r\n");
      out.write("                    <div id=\"center_tab-1\" class=\"center_tab-content current\">\r\n");
      out.write("                    \t<table class=\"service_center_table\">\r\n");
      out.write("\t\t\t\t\t\t    <tr class=\"service_center_top_tr\">\r\n");
      out.write("\t\t\t\t\t\t        <td style=\"width: 110px;\">번호</td>\r\n");
      out.write("\t\t\t\t\t\t        <td style=\"width: 120px;\">카테고리</td>\r\n");
      out.write("\t\t\t\t\t\t        <td>제목</td>\r\n");
      out.write("\t\t\t\t\t\t        <td style=\"width: 190px;\">게시일</td>\r\n");
      out.write("\t\t\t\t\t\t    </tr>\r\n");
      out.write("\t\t\t\t\t\t    <tr class=\"service_center_middle_tr\">\r\n");
      out.write("\t\t\t\t\t\t        <td>2</td>\r\n");
      out.write("\t\t\t\t\t\t        <td>제품</td>\r\n");
      out.write("\t\t\t\t\t\t        <td> <span class=\"glyphicon table-plus center_faq\">제품 등급제란?</span>\r\n");
      out.write("\t\t\t\t\t\t            <span class=\"glyphicon table-minus center_faq\" style=\"display:none;\">제품 등급제란?</span></td>\r\n");
      out.write("\t\t\t\t\t\t        <td>2020.01.11</td>\r\n");
      out.write("\t\t\t\t\t\t    </tr>\r\n");
      out.write("\t\t\t\t\t\t    <tr class=\"center_faq_hidden_content\">\r\n");
      out.write("\t\t\t\t\t\t        <td></td>\r\n");
      out.write("\t\t\t\t\t\t        <td></td>\r\n");
      out.write("\t\t\t\t\t\t        <td class=\"center_faq_hidden_detail\">제품등급은 상품을 육안으로 볼 수 없는 고객을 위해 가늠할 수 있는 상태를 표기하고 있습니다.<br>\r\n");
      out.write("\t\t\t\t\t\t            더 자세한 사항은 상품별 설명과 이미지를 참고하시기 바랍니다.<br><br>\r\n");
      out.write("\t\t\t\t\t\t            NS - 새상품 또는 새상품과 동일한 상태이나 개봉한 상태이며 부속품이 일부 없을 수 있음<br>\r\n");
      out.write("\t\t\t\t\t\t            A - 외관상 자연스러운 사용감 있음, 전체적으로 상태가 양호한 상품<br>\r\n");
      out.write("\t\t\t\t\t\t            B - 스크래치, 구김, 때 등 사용흔적 있는 편으로 외관상 확연한 중고느낌 있음<br>\r\n");
      out.write("\t\t\t\t\t\t        <td></td>\r\n");
      out.write("\t\t\t\t\t\t    </tr>\r\n");
      out.write("\t\t\t\t\t\t    <tr class=\"service_center_middle_tr\">\r\n");
      out.write("\t\t\t\t\t\t        <td>1</td>\r\n");
      out.write("\t\t\t\t\t\t        <td>회원가입</td>\r\n");
      out.write("\t\t\t\t\t\t        <td> <span class=\"glyphicon table-plus center_faq\">재가입은 어떻게 하나요?</span>\r\n");
      out.write("\t\t\t\t\t\t            <span class=\"glyphicon table-minus center_faq\" style=\"display:none\">재가입은 어떻게 하나요?</span>\r\n");
      out.write("\t\t\t\t\t\t        </td>\r\n");
      out.write("\t\t\t\t\t\t        <td>2020.01.11</td>\r\n");
      out.write("\t\t\t\t\t\t    </tr>\r\n");
      out.write("\t\t\t\t\t\t    <tr class=\"center_faq_hidden_content\">\r\n");
      out.write("\t\t\t\t\t\t        <td></td>\r\n");
      out.write("\t\t\t\t\t\t        <td></td>\r\n");
      out.write("\t\t\t\t\t\t        <td class=\"center_faq_hidden_detail\">재가입은 탈퇴후에도 즉시 하실 수 있습니다.<br><br>\r\n");
      out.write("\t\t\t\t\t\t            단, 탈퇴처리 과정에 있는 아이디로는 동일하게 가입하실 수 없으니<br>\r\n");
      out.write("\t\t\t\t\t\t            다른 아이디를 생성하셔야만 가능합니다.<br><br>\r\n");
      out.write("\t\t\t\t\t\t            또한 동일한 아이디를 사용하기 위해서는 고객센터로 이전 아이디에 대한<br>\r\n");
      out.write("\t\t\t\t\t\t            삭제요청을 하신후에 재가입이 가능합니다.<br>\r\n");
      out.write("\t\t\t\t\t\t            하지만 이때 이전에 남아있던 포인트나 활동내역에 대해서는 복구 받으실 수 없습니다.</td>\r\n");
      out.write("\t\t\t\t\t\t        <td></td>\r\n");
      out.write("\t\t\t\t\t\t    </tr>\r\n");
      out.write("\t\t\t\t\t\t</table>\r\n");
      out.write("                    </div>\r\n");
      out.write("                <br/><br/><br/>\r\n");
      out.write("                </div>\r\n");
      out.write("            </section>\r\n");
      out.write("        </div>\r\n");
      out.write("    </section>\r\n");
      out.write("\r\n");
      out.write("\t<footer id=\"footer\"></footer>\r\n");
      out.write("</body>\r\n");
      out.write("\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
